# This file exports every derivation introduced by this repository.
{ nixpkgs ? import ./nixpkgs.nix }:
let pkgs = import ./pkgs.nix { inherit nixpkgs; };
in
pkgs.releaseTools.aggregate {
  name = "everything";
  constituents = [
    pkgs._here.ghc882.xmlbf
    pkgs._here.ghc882.xmlbf.doc
    pkgs._here.ghc882.xmlbf-xeno
    pkgs._here.ghc882.xmlbf-xeno.doc
    pkgs._here.ghc882.xmlbf-xmlhtml
    pkgs._here.ghc882.xmlbf-xmlhtml.doc
    pkgs._here.ghc882._shell
  ];
}

